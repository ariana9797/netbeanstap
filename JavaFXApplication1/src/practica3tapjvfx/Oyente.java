/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica3tapjvfx;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.swing.JFileChooser;

/**
 * FXML Controller class
 *
 * @author arian
 */
public class Oyente implements Initializable {

    @FXML
    private TextField ruta;
    @FXML
    private Button buscar;
  //  private Stage stage;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void click(ActionEvent event) {
     //   stage = primaryStage;
        JFileChooser seleccionado = new JFileChooser(".");
        if(seleccionado.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
            File fichero=seleccionado.getSelectedFile();
            //Ecribe la ruta del fichero seleccionado en el campo de texto
            ruta.setText(fichero.getAbsolutePath());
        }
    }
    
}
