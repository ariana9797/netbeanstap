/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author arian
 */
public class Oyente implements Initializable {

    @FXML
    private TextField nombre;
    @FXML
    private Button saludar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void click(ActionEvent event) {
        try{
            String nombret = this.nombre.getText();
            //String nombre = panel.getNombre().getText();
            JOptionPane.showMessageDialog(null, "¡Hola "+ nombret+"!");
            this.nombre.setText("");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        //panel.getNombre().setText("");
    }
    
}
