/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2tapjfxx;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author arian
 */
public class Oyente implements Initializable {

    @FXML
    private TextField resulttado;
    @FXML
    private Spinner <Integer>numero1;
    @FXML
    private Spinner <Integer>numero2;
    SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(-100, 100, 0);
    SpinnerValueFactory<Integer> valueFactory2 = new SpinnerValueFactory.IntegerSpinnerValueFactory(-100, 100, 0);
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //initSpinner();
        this.numero1.setValueFactory(valueFactory);
        this.numero2.setValueFactory(valueFactory2);
    }    

    @FXML
    private void click(ActionEvent event) {
        int num1 = numero1.getValue();
        int num2 = numero2.getValue();
        String res = Integer.toString((int)(Math.random()* (num2-num1+1) + num1));
        resulttado.setText(res);
        //int num1 = numero1.getValue();
        //int num2 = numero2.getValue();
       // System.out.println(num1);
       // System.out.println(num2);
        //int num1 = (int) numero1.getValue();
        //int num2 = (int) numero2.getValue();
        //Random rnd = new Random();
        //String res = Integer.toString((int)(Math.random()* (num2-num1+1) + num1));//Integer.toString((int)(Math.random()* num2 + num1));
        //resulttado.setText(res);
    }
    
}
