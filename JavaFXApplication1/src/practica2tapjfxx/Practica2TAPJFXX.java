/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2tapjfxx;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author arian
 */
public class Practica2TAPJFXX extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Practica2TAPJFXX.class.getResource("PanelNumeros.fxml"));
            Pane ventana = (Pane) loader.load();
            Scene scene = new Scene(ventana);
        
            primaryStage.setTitle("Practica 2");
            primaryStage.setScene(scene);
            primaryStage.show();
        }catch(IOException e){
           System.out.println(e.getMessage()); 
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
