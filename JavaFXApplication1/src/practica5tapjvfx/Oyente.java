/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica5tapjvfx;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javax.swing.JFileChooser;

/**
 * FXML Controller class
 *
 * @author arian
 */
public class Oyente implements Initializable {

    @FXML
    private MenuItem abrir;
    @FXML
    private MenuItem salir;
    @FXML
    private TextField ruta;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void clickAbrir(ActionEvent event) {
        JFileChooser seleccionado = new JFileChooser(".");
                if(seleccionado.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                    File fichero=seleccionado.getSelectedFile();
                    //Ecribe la ruta del fichero seleccionado en el campo de texto
                    ruta.setText(fichero.getAbsolutePath());
                }  
    }

    @FXML
    private void clickSalir(ActionEvent event) {
         System.exit(0);
    }
    
}
