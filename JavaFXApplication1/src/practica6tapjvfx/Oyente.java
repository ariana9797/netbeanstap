/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica6tapjvfx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javax.swing.JComponent;

/**
 * FXML Controller class
 *
 * @author arian
 */
public class Oyente implements Initializable {

    @FXML
    private RadioButton op1;
    @FXML
    private RadioButton op2;
    @FXML
    private RadioButton op3;
    @FXML
    private CheckBox op4;
    @FXML
    private CheckBox op5;
    @FXML
    private CheckBox op6;
    @FXML
    private TextField texto1;
    @FXML
    private ComboBox<?> combo1;
    @FXML
    private Spinner<?> valores1;
    @FXML
    private RadioButton ope1;
    @FXML
    private RadioButton ope2;
    @FXML
    private RadioButton ope3;
    @FXML
    private CheckBox ope4;
    @FXML
    private CheckBox ope5;
    @FXML
    private CheckBox ope6;
    @FXML
    private ComboBox<?> combo2;
    @FXML
    private Spinner<?> valores2;
    @FXML
    private TextField texto2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ToggleGroup toggleGroup = new ToggleGroup();
        ToggleGroup toggleGroup2 = new ToggleGroup();
        op1.setToggleGroup(toggleGroup);
        op2.setToggleGroup(toggleGroup);
        op3.setToggleGroup(toggleGroup);
        
        ope1.setToggleGroup(toggleGroup2);
        ope2.setToggleGroup(toggleGroup2);
        ope3.setToggleGroup(toggleGroup2);
        
        // TODO
    }    

    @FXML
    private void click(ActionEvent event) {
        //JComponent origen = (JComponent)e.getSource();
//        
//        switch(origen.getName()){
//            case "opc1":
//                ope1.setSelected(true);
//                break;
//            case "opc2":
//                ope2.setSelected(true);
//                break;
//            case "opc3":
//                ope3.setSelected(true);
//                break;
//            case "opc4":
//                ope4.setSelected(op4.isSelected());
//                break;
//            case "opc5":
//                ope5.setSelected(op5.isSelected());
//                break;
//            case "opc6":
//                ope6.setSelected(op6.isSelected());
//                break;  
//            case "tex1":
//                texto2.setText(texto1.getText());
//                break;
//            case "comb1":
//                combo2.setSelectedIndex(combo1.getSelectedIndex());
//                break;
//          
//            default:
//        }
    }
    
    
}
