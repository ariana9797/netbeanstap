/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica4tapjvfx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author arian
 */
public class Oyente implements Initializable {

    @FXML
    private TextField titiuloPelicula;
    @FXML
    private ComboBox<String> peliculas;
    @FXML
    private Button nuevaPelicula;
    //Me notifica cuando se agrega o elimina un elemento de la lista
    ObservableList<String> items = FXCollections.observableArrayList();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void click(ActionEvent event) {
        String peli = titiuloPelicula.getText();
        peliculas.getItems().addAll(peli);
        //ObservableList<String> items = FXCollections.observableArrayList();
        //peliculas.idProperty();
        
        //peliculas.addItem(peli);
        titiuloPelicula.setText("");
    }
    
}
